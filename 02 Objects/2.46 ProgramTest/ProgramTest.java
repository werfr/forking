
/**
 Simple program that compares real and expected values to test the
 behavior of a method.
*/
public class ProgramTest
{
  private static final int X_GROWTH = 20;
  private static final int Y_GROWTH = 20;

  public static void main(String[] args)
  {
    Rectangle box = new Rectangle(45, 90, 60, 90);
    int expectedX = box.getX() - X_GROWTH;
    int expectedY = box.getY() - Y_GROWTH;
    int expectedW = box.getWidth()  + X_GROWTH * 2;
    int expectedH = box.getHeight() + Y_GROWTH * 2;

    box.grow(X_GROWTH, Y_GROWTH);
    System.out.println(box.getX());
    System.out.println("Expected: " + expectedX);
    System.out.println(box.getY());
    System.out.println("Expected: " + expectedY);
    System.out.println(box.getWidth());
    System.out.println("Expected: " + expectedW);
    System.out.println(box.getHeight());
    System.out.println("Expected: " + expectedH);
  }
}
