
/**
  A simple program to check the use of String#replace.
*/
public class Mississippi
{
  public static void main(String args[])
  {
    String river = "Mississippi";
    System.out.println(river.replace('i', 'x'));
  }
}
