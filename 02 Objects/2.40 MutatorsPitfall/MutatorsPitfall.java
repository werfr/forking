
/**
  Simple program that shows how mutators can affect references to
  objects in ways we may not expect.
*/
public class MutatorsPitfall
{
  public static void main(String[] args)
  {
    Rectangle box1 = new Rectangle(5, 10, 60, 90);
    Rectangle box2 = box1;

    box1.translate(100, 100);
    System.out.println(box1.getX());
    System.out.println(box2.getX());
  }
}
